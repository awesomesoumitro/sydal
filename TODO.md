# TODO

1. Create editor scaffold.  
1.1. Use [CodeMirror](https://codemirror.net).  
1.2. Use [keybindings](https://codemirror.net/doc/manual.html#keymaps) to create run channel bindings.  
1.3. Use following addons in CodeMirror.  
1.3.1. [edit/closebrackets.js](https://codemirror.net/addon/edit/closebrackets.js)  
1.3.2. [fold/foldcode.js](https://codemirror.net/addon/edit/closebrackets.js)  
1.3.3. [lint/lint.js](https://codemirror.net/addon/lint/lint.js)  
1.4. Use [gutter events/decoration](https://codemirror.net/doc/manual.html#api_decoration) to create actions for each channel.  
1.4.1. Actions are: play, pause, mute, delete.  
2. Create player API.  
2.1. Use [Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API) to create audio output bindings.  
2.2. Play [multiple sounds](http://chimera.labs.oreilly.com/books/1234000001552/ch04.html#s04_2s) and other combinations.  
2.3. Create and serve samples library (using [Dirt-Samples](https://github.com/tidalcycles/Dirt-Samples/tree/c2db9a0dc4ffb911febc613cdb9726cae5175223)?)  
2.4. Create commands for all creating `audio contexts`, applying `filters` and `transforms` using the *Web Audio API*.  
3. Create parser language.  
3.1. Other parser references:  
3.1.1. [ctford/klangmeister](https://github.com/ctford/klangmeister/blob/master/src/klangmeister/compile/eval.cljs) (*to use this*)  
3.1.2. [tidalcycles/Tidal](https://github.com/tidalcycles/Tidal/tree/master/Sound/Tidal)  

## Further 
1. OSC Support ([OSC.js](https://github.com/colinbdclark/osc.js/))  
2. Music Theory Support ([Teoria](https://github.com/saebekassebil/teoria))  
3. Faster DSP Support ([Gibberish](https://github.com/charlieroberts/Gibberish)) 