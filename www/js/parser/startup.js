function SydalParser() {
	return {
		evaluate: function(code) {
			this.lint(code);
		},

		lint: function(code) {
			// get esprima to check the code and get the ast
			try {
				esprima.parse(code, { loc: true });
			} catch(error) {
				document
					.getElementById('sydalerrors')
					.innerHTML = "Line "
							   + error.lineNumber
							   + " ("
							   + error.column
							   + "): "
							   + error.description
							   ;
				return;																
			}
			this.compile(code);
		},

		compile: function(code) {
			// TODO: Use a safer eval function
			try {
				eval(code);
			} catch(error) {
				document
					.getElementById('sydalerrors')
					.innerHTML = "Line "
							   + error.lineNumber
							   + " ("
							   + error.column
							   + "): "
							   + error.message
							   ;
				return;
			}
			window.SydalChannels = [];
			for(var f in window) {
				if(window[f] instanceof SydalChannel) {
					window.SydalChannels[f] = window[f];
				}
			}
			window.player.channelInit(window.SydalChannels);
		}
	}
}

function SydalChannel(pattern, transforms = {}) {
	this.pattern = pattern;
	this.transforms = transforms;

	this.showDetails = function() {
		console.log("Pattern: " + this.pattern);
		console.log("Transforms: " + this.transforms);
	}

	this.initSamples = function() {
		var samples = [];
		var sampleArr = this.pattern.split(" ");
		sampleArr.forEach(function (x) {
			ntPair = x.split("*");
			samples.push({ 
				  name: ntPair[0]
				, times: ntPair[1] == undefined ? 1 : ntPair[1]
			});
		});
		return samples;
	}
	this.samples = this.initSamples();

	this.gain = function(atr) {
		// TODO
		transforms.gain = atr;
		return this;
	}

	this.pan = function(atr) {
		// TODO
		transforms.pan = atr;
		return this;
	}
}

window.sound = function(pattern) {
	// window.parser.sendCommands();
	console.log("Made Channel: " + pattern)
	return new SydalChannel(pattern);
}

// remember, player has the actual state