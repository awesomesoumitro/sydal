var samplesState = [];
var channelState = [];
var number = 1200;
var filtertype = 'lowpass';

function change(z){
	number = z;
}

function filtertypes(d){
	if(d == 1)
	{
		filtertype = 'lowpass'
	}

	else if (d == 2)
	{
		filtertype = 'highpass'
	}

	else if (d == 3)
	{
		filtertype = 'bandpass'
	}

	else if (d == 4)
	{
		filtertype = 'lowshelf'
	}

	else if (d == 5)
	{
		filtertype = 'lowpass'
		number = 1200;
	}
}

function SydalPlayer() {
    var context, nextBeat;
    return {
        start: function() {
            context = new (window.AudioContext || window.webkitAudioContext)();
            console.log("Created Audio Context");
            this.getSamplesList(window.editor.populateSamplesList);
            nextBeat = context.currentTime;
            setInterval(function () {
                if(context.currentTime - nextBeat >= 2.4) {
                    nextBeat = context.currentTime;
                }
            }, 100);
        },

        getSamplesList: function(cb) {
            var request = new XMLHttpRequest();
            request.open('GET', '/samples.json', true);
            request.onreadystatechange = _populateSamplesList;

            function _populateSamplesList() {
                if (request.readyState === XMLHttpRequest.DONE) {
                    if (request.status === 200) {
                        window.samples = JSON.parse(request.responseText);
                        cb();
                    }
                    else {
                        console.error("Couldn't fetch samples list.");
                    }
                }
            }
            request.send();
        },

        loadSample: function(sampleName) {
            // TODO: Check if sample is present in sampleState
            // TODO: Check if sample is present in sampleList

            // Download sample
            var request = new XMLHttpRequest();
            request.open("GET", '/samples/' + sampleName, true);
            request.responseType = "arraybuffer";
            request.onreadystatechange = _loadSample;
            
            function _loadSample() {
                if (request.readyState === XMLHttpRequest.DONE) {
                    if (request.status === 200) {
                        context.decodeAudioData(
                            request.response,
                            function(buffer) {
                                if (!buffer) {
                                    console.error('error decoding file data:', url);
                                    return;
                                }
                                console.log("("+sampleName+") Added")
                                window.samplesState[sampleName] = buffer;
                            },
                            function(error) {
                                console.error('decodeAudioData error:', url);
                                return;
                            }
                        );
                    }
                    else {
                        console.error("Couldn't fetch sample "+ sampleName +".");
                    }
                }
            }
            request.send();
        },

        channelInit: function(sydalChannels) {
            // For every channel
            for(var chan in sydalChannels) {
                var _loadedSamplesCount = 0;
                var _player = this;

                // For every sample in channel
                sydalChannels[chan].samples.forEach(function(sample) {
                    // Load the sample
                    _player.loadSample(sample.name);
                });
                if(channelState[chan] == undefined) {
                    channelState[chan] = {};
                    // Initialise channel state for the first time
                    channelState[chan].intervalObj = setInterval(function() {
                        _player.playChannel(chan, sydalChannels);
                    }, 2400);
                }
                // Or on pattern state change
                else if(channelState[chan].channelProps.pattern != sydalChannels[chan].pattern) {
                    if(channelState[chan] != undefined)
                        // Reset Interval
                        clearInterval(channelState[chan].intervalObj);
                    // Create setInterval to loop
                    channelState[chan].intervalObj = setInterval(function() {
                        _player.playChannel(chan, sydalChannels);
                    }, 2400);
                }
            }
        },

        playChannel: function(chan, sydalChannels) {
            channelState[chan].channelProps = sydalChannels[chan];
            channelState[chan].bufferSource = [];
            var filter = [] ;
            var gainNode = [];

            var j = 0;
            sydalChannels[chan].samples.forEach(function(sample) {
                for(var i = 0; i < sample.times; i++) {
                	filter[j]   = context.createBiquadFilter();//create filter node
                	gainNode[j] = context.createGain();//create gain
                    channelState[chan].bufferSource[j] = context.createBufferSource();
                    channelState[chan].bufferSource[j].buffer = samplesState[sample.name];
                    channelState[chan].bufferSource[j].connect(gainNode[j]);//connect gain Node
                    gainNode[j].connect(filter[j]);//connect filter
                    filter[j].connect(context.destination);//connect destination
                    gainNode[j].gain.value = sydalChannels[chan].transforms.gain || gainNode[j].gain.defaultValue;
                    // TODO provide .filter()
                    filter[j].type = filtertype; // Low-pass filter. See BiquadFilterNode docs 
                    filter[j].frequency.value = number; // Set cutoff to 440 Hz
                    j++;
                }
            });
            while(context.currentTime < nextBeat);
            gap = context.currentTime;
            for(var k = 0; k < j; k++){
                gap += 0.6;
                channelState[chan].bufferSource[k].start(gap);
            }
        }

    }
}