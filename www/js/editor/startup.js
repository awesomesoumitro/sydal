function SydalEditor() {
		var editor;
	return {
		start: function() {
			editor = CodeMirror.fromTextArea(
				document.getElementById("sydalcode")
				, {
				  	mode: "javascript",
			        lineNumbers: true,
					theme : "twilight",
					lineWrapping: true,
					autofocus: true,
					lint: true,
					styleActiveLine: true,
					autoCloseBrackets: true,
					extraKeys: {"Ctrl-Space": "autocomplete",
								"Ctrl-Q": function(cm) { cm.foldCode(cm.getCursor()); },
							   },
			        foldGutter: true,
			        gutters: [ "CodeMirror-linenumbers"
			        		 , "CodeMirror-foldgutter"
			        		 , "CodeMirror-lint-markers"],
			    });
	  		// editor.foldCode(CodeMirror.Pos(7, 0));
	   		// map = {"Alt-Space": a,
				   // "Ctrl-/"   : b};

			// editor.addKeyMap(map);
			
			// function b(cm) {
			// 	console.log("byre");
			// }
			
			editor.on("change", this.evaluate);
		},

		evaluate: function(cm) {
			window.parser.evaluate(cm.getValue()); // use global parser service
		},

		insertSample: function(sample) {
        	var selection = editor.getSelection();

	        if(selection.length > 0){
	            editor.replaceSelection(sample);
	        }
	        else {
	            var doc = editor.getDoc();
	            var cursor = doc.getCursor();

	            var pos = {
	               line: cursor.line,
	               ch: cursor.ch
	            }
	            doc.replaceRange(sample, pos);
	        }

	    },

		populateSamplesList: function() {
			var listContainer = document.getElementById('sampleslist');
			var html = "";

			window.samples.children.forEach(function traverse(node) {
				html += "<div class='in'>+ ";
				if(node.type == "file")
					html += "<a onClick='window.editor.insertSample(\""
						  + node.path.substr(8)
						  + "\")'>"
						  + node.name
						  + "</a>";
				else
					html += node.name;
				html += "<br>";
				if(node.type == "directory")
					node.children.forEach(traverse);
				html += "</div>"
			});

			listContainer.innerHTML = html;
		}
	}
}
     