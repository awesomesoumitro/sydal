var parser, player, editor;
(function() {
	editor = new SydalEditor();
	editor.start(); // start editor
	
	player = new SydalPlayer();
	player.start(); // start player

	parser = new SydalParser();
})();