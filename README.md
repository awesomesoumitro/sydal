# Sydal
Sydal is an easy to use web application that would seamlessly integrate the livecoding experience with an easy to learn syntax. The solution is to have a alternative that is light enough to deploy completely on a web browser, thereby eliminating any machine dependence and enough to perform simple music on livecoding production environment.

Samples server for Sydal uses [tidalcycles/Dirt-Samples](https://github.com/tidalcycles/Dirt-Samples) to keep some samples handy. Also has `/samples.json` endpoint to detail about the samples available.

## Install
	git submodule update samples
    npm install
    
## Run
    npm start
    open http://localhost:8080/
    
## Contributors
T Vamsi Sai  
Shivam Malik  
Soumitro Chakraborty  