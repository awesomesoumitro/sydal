var express = require('express');
var dirTree = require('directory-tree');

var app = express();
app.use('/samples', express.static('samples'));
app.use(express.static('www'));

app.get('/samples.json', function (req, res) {
  var samplesTree = dirTree('samples');
  res.json(samplesTree);
});

app.listen(8080, function () {
  console.log('Samples Server listening on port 8080!');
});